function removeDuplicates(numbers) {
   
    const uniqueNumbers = [...new Set(numbers)];
    
    
    console.log('Input array:', numbers);
    console.log('Array with unique numbers:', uniqueNumbers);
    
   
    return uniqueNumbers;
}


const numbers = [1, 2, 2, 3, 4, 4, 5];
const uniqueNumbers = removeDuplicates(numbers);